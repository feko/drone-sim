﻿using droneSim.Data.Models;
using System;

namespace droneSim.UI.Interfaces
{
    public interface IUIMenu
    {
        UIMenu UIMenu { get; }
        UIMenu ParentMenu { get; }

        event EventHandler OnMenuShown;
        event EventHandler OnMenuHidden;

        void Show();
        void Hide();
    }
}
