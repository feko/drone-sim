﻿namespace droneSim.UI.Interfaces
{
    public interface IUIAnimatedComponent
    {
        void AnimateOnFocusGained();
        void AnimateOnFocusLost();
        void ResetState();
    }
}
