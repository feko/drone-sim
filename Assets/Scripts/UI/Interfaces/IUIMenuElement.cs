﻿using droneSim.Input.Interfaces;

namespace droneSim.UI.Interfaces
{
    public interface IUIMenuElement : IFocusable
    {
        void OnSelect();
    }
}
