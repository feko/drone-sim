﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.UI.Interfaces;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace droneSim.UI.Implementations
{
    public class BasicUIMenuElement : MonoBehaviour, IUIMenuElement
    {
        protected readonly List<IUIAnimatedComponent> _animatedComponents = new List<IUIAnimatedComponent>();
        protected IAudioController _audioController;

        [Inject]
        protected void Inject(List<IUIAnimatedComponent> animatedComponents, 
            IAudioController audioController)
        {
            _animatedComponents.AddRange(animatedComponents);
            _audioController = audioController;
        }

        public virtual void OnFocusGained()
        {
            _audioController.PlaySound(Data.Models.SoundType.Focus);
            transform.localScale *= 1.3f;
            _animatedComponents.ForEach(ac => ac.AnimateOnFocusGained());
        }

        public virtual void OnFocusLost()
        {
            transform.localScale /= 1.3f;
            _animatedComponents.ForEach(ac => ac.AnimateOnFocusLost());
        }

        public virtual void OnSelect()
        {
            _audioController.PlaySound(Data.Models.SoundType.Select);
            _animatedComponents.ForEach(ac => ac.ResetState());
        }
    }
}
