﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations
{
    public class BasicUIAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        protected Color _baseColor;
        protected Color _highlightedColor;

        protected Renderer _renderer;

        protected virtual void Awake()
        {
            _renderer = GetComponent<Renderer>();
            _baseColor = _renderer.material.color;
            ColorUtility.TryParseHtmlString("#4483EDFF", out _highlightedColor);
        }

        public virtual void AnimateOnFocusGained()
        {
            _renderer.material.color = _highlightedColor;
        }

        public virtual void AnimateOnFocusLost()
        {
            _renderer.material.color = _baseColor;
        }

        public virtual void ResetState()
        {
            _renderer.material.color = _baseColor;
        }
    }
}
