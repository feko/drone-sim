﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using droneSim.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.UI.Implementations
{
    public class BasicUIMenu : MonoBehaviour, IUIMenu
    {
        [SerializeField]
        private UIMenu _uIMenu;
        public virtual UIMenu UIMenu => _uIMenu;

        [SerializeField]
        private UIMenu _parentMenu;
        public virtual UIMenu ParentMenu => _parentMenu;

        public event EventHandler OnMenuShown;
        public event EventHandler OnMenuHidden;

        protected readonly List<IUIMenuElement> _menuElements = new List<IUIMenuElement>();

        protected IFocusController _focusController;
        private IAudioController _audioController;
        private Vector3 _initialLocalScale;

        [Inject]
        protected void Inject(List<IUIMenuElement> uIMenuElements,
            IFocusController focusController,
            IAudioController audioController)
        {
            _menuElements.AddRange(uIMenuElements);
            _focusController = focusController;
            _audioController = audioController;
        }

        protected virtual void Awake()
        {
            gameObject.SetActive(false);
            _initialLocalScale = transform.localScale;
        }

        public virtual void Hide()
        {
            if (!LeanTween.isTweening(gameObject))
            {
                _focusController.UpdateFocusableObjects(Enumerable.Empty<IFocusable>());
                transform.localScale = _initialLocalScale;
                _audioController.PlaySound(SoundType.SwingDown);
                LeanTween.cancel(gameObject);
                LeanTween.scale(gameObject, Vector3.zero, 0.25f)
                    .setEaseOutQuint()
                    .setOnComplete(() =>
                    {
                        gameObject.SetActive(false);
                        OnMenuHidden?.Invoke(this, EventArgs.Empty);
                    });
            }
        }

        public virtual void Show()
        {
            if (!LeanTween.isTweening(gameObject))
            {
                transform.localScale = Vector3.zero;
                gameObject.SetActive(true);
                _audioController.PlaySound(SoundType.SwingUp);
                LeanTween.cancel(gameObject);
                LeanTween.scale(gameObject, _initialLocalScale, 0.25f)
                    .setEaseOutQuint()
                    .setOnComplete(() =>
                    {
                        _focusController.UpdateFocusableObjects(_menuElements.Cast<IFocusable>());
                        OnMenuShown?.Invoke(this, EventArgs.Empty);
                    });
            }
        }
    }
}