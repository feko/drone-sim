﻿using droneSim.Input.Interfaces;
using System.Collections.Generic;
using UnityEngine;

namespace droneSim.UI.Implementations
{
    public class OnFocusObstacleHighlighter : MonoBehaviour, IFocusable
    {
        [SerializeField]
        private Material _highlightMaterial;

        private Vector3 _initialScale = Vector3.zero;
        private readonly Dictionary<Renderer, Material> _initialMaterials = new Dictionary<Renderer, Material>();
        private Renderer[] _renderers;

        private void Awake()
        {
            _renderers = GetComponentsInChildren<Renderer>(true);
        }

        public void OnFocusGained()
        {
            if (_initialScale == Vector3.zero)
            {
                _initialScale = transform.localScale;
            }
            _initialMaterials.Clear();
            LeanTween.cancel(gameObject);
            LeanTween.scale(gameObject, _initialScale * 1.3f, 0.5f)
                .setEaseOutQuint();
            foreach (var renderer in _renderers)
            {
                _initialMaterials.Add(renderer, renderer.material);
                renderer.material = _highlightMaterial;
            }
        }

        public void OnFocusLost()
        {
            LeanTween.cancel(gameObject);
            LeanTween.scale(gameObject, _initialScale, 0.5f)
                .setEaseOutQuint();
            foreach (var renderer in _renderers)
            {
                renderer.material = _initialMaterials[renderer];
            }
        }
    }
}
