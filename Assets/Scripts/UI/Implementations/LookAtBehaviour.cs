﻿using UnityEngine;

namespace droneSim.UI.Implementations
{
    public class LookAtBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Transform _objectToLookAt;

        private void Update()
        {
            transform.LookAt(_objectToLookAt.position);
        }
    } 
}
