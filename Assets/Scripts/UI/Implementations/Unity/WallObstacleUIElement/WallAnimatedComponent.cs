﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.WallObstacleUIElement
{
    public class WallAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        private Vector3 _initialLocalScale;
        [SerializeField]
        private AnimationCurve _animationCurve;

        private void Awake()
        {
            _initialLocalScale = transform.localScale;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            transform.localScale = Vector3.zero;
            LeanTween.scale(gameObject, _initialLocalScale, 1f)
                .setEase(_animationCurve);
        }

        public void AnimateOnFocusLost()
        {
            ResetState();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localScale = _initialLocalScale;
        }
    }
}
