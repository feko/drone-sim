﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.FlyDroneUIElement
{
    public class DroneAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        private Vector3 _initialLocalPosition;
        private Quaternion _initialLocalRotation;

        private void Awake()
        {
            _initialLocalPosition = transform.localPosition;
            _initialLocalRotation = transform.localRotation;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            LeanTween
                .rotateLocal(gameObject, _initialLocalRotation.eulerAngles + Vector3.up * 180, 1f)
                .setEaseOutExpo();
            LeanTween
                .moveLocal(gameObject, _initialLocalPosition + Vector3.up * 0.125f, 1f)
                .setEaseOutExpo();
        }

        public void AnimateOnFocusLost()
        {
            LeanTween.cancel(gameObject);
            LeanTween
                .rotateLocal(gameObject, _initialLocalRotation.eulerAngles, 1f)
                .setEaseOutExpo();
            LeanTween
                .moveLocal(gameObject, _initialLocalPosition, 1f)
                .setEaseOutExpo();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localPosition = _initialLocalPosition;
            transform.localRotation = _initialLocalRotation;
        }
    }
}
