﻿using droneSim.Controllers.Interfaces;
using Zenject;

namespace droneSim.UI.Implementations.Unity.FlyDroneUIElement
{
    public class FlyDroneUIElement : BasicUIMenuElement
    {
        IApplicationController _applicationController;

        [Inject]
        private void Inject(IApplicationController applicationController)
        {
            _applicationController = applicationController;
        }

        public override void OnSelect()
        {
            base.OnSelect();
            _applicationController.StartFlyingDrone(transform);
        }
    }
}