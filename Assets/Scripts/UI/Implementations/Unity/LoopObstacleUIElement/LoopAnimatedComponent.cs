﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.LoopObstacleUIElement
{
    public class LoopAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        private Quaternion _initialLocalRotation;

        private void Awake()
        {
            _initialLocalRotation = transform.localRotation;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            LeanTween.rotateLocal(gameObject, _initialLocalRotation.eulerAngles + new Vector3(0f, 180f, 0), 1f)
                .setEaseOutElastic();
        }

        public void AnimateOnFocusLost()
        {
            ResetState();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localRotation = _initialLocalRotation;
        }
    }
}