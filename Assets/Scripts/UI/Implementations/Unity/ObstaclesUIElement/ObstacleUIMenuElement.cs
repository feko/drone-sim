﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using UnityEngine;
using Zenject;

namespace droneSim.UI.Implementations.Unity.ObstaclesUIElement
{
    public class ObstacleUIMenuElement : BasicUIMenuElement
    {
        [SerializeField]
        private ObstacleType _obstacleType;

        private IApplicationController _applicationController;

        [Inject]
        private void Inject(IApplicationController applicationController)
        {
            _applicationController = applicationController;
        }

        public override void OnSelect()
        {
            base.OnSelect();
            _applicationController.StartPlacingObstacles(_obstacleType);
        }
    }
}
