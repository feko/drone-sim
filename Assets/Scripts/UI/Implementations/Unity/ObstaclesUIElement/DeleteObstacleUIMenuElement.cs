﻿using droneSim.Controllers.Interfaces;
using Zenject;

namespace droneSim.UI.Implementations.Unity.ObstaclesUIElement
{
    public class DeleteObstacleUIMenuElement : BasicUIMenuElement
    {
        private IApplicationController _applicationController;
        private IObstaclePlacingController _obstaclePlacingController;

        [Inject]
        private void Inject(IApplicationController applicationController,
            IObstaclePlacingController obstaclePlacingController)
        {
            _applicationController = applicationController;
            _obstaclePlacingController = obstaclePlacingController;
        }

        public override void OnSelect()
        {
            if (_obstaclePlacingController.ObstaclesPlaced)
            {
                base.OnSelect();
                _applicationController.StartDeletingObstacles();
            }
            else
            {
                _audioController.PlaySound(Data.Models.SoundType.Error);
                _animatedComponents.ForEach(ac => ac.ResetState());
            }
        }

        public override void OnFocusGained()
        {
            base.OnFocusGained();
        }
    }
}
