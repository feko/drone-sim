﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using Zenject;

namespace droneSim.UI.Implementations.Unity.ObstaclesUIElement
{
    public class ObstacleUIElement : BasicUIMenuElement
    {
        private IUIController _uIController;

        [Inject]
        protected void Inject(IUIController uIController)
        {
            _uIController = uIController;
        }

        public override void OnSelect()
        {
            base.OnSelect();
            _uIController.Show(UIMenu.Obstacles);
        }
    }
}
