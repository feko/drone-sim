﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.ObstaclesUIElement
{
    public class ObstacleAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        [SerializeField]
        private float _delay = 0;

        private Vector3 _initialLocalScale;

        private void Awake()
        {
            _initialLocalScale = transform.localScale;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            transform.localScale = Vector3.zero;
            LeanTween.scale(gameObject, _initialLocalScale, 1f)
                .setEaseOutElastic()
                .setDelay(_delay);
        }

        public void AnimateOnFocusLost()
        {
            ResetState();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localScale = _initialLocalScale;
        }
    }
}
