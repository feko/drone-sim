﻿#if !UNITY_EDITOR
using UnityEngine;
#endif

namespace droneSim.UI.Implementations.Unity.ExitUIElement
{
    public class ExitUIElement : BasicUIMenuElement
    {
        public override void OnSelect()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
