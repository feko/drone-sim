﻿using droneSim.UI.Interfaces;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.ExitUIElement
{
    public class DroneAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        [SerializeField]
        private float _delay;

        private Vector3 _initialLocalPosition;
        private Quaternion _initialLocalRotation;
        private Vector3 _initialLocalScale;

        private void Awake()
        {
            _initialLocalPosition = transform.localPosition;
            _initialLocalRotation = transform.localRotation;
            _initialLocalScale = transform.localScale;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            LeanTween.moveLocal(gameObject, _initialLocalPosition + new Vector3(0.06f, 0f, 0.1625f), 1f)
                .setEaseInBack();
            LeanTween.rotateLocal(gameObject, _initialLocalRotation.eulerAngles + new Vector3(-25f, 0f, 0f), 1f)
                .setEaseOutCubic();
            LeanTween.scale(gameObject, Vector3.zero, 0.5f)
                .setEaseInQuint()
                .setOnComplete(() => gameObject.SetActive(false))
                .setDelay(0.5f);
        }

        public void AnimateOnFocusLost()
        {
            ResetState();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localPosition = _initialLocalPosition;
            transform.localRotation = _initialLocalRotation;
            transform.localScale = _initialLocalScale;
            gameObject.SetActive(true);
        }
    }
}
