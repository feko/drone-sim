﻿using droneSim.UI.Interfaces;
using System;
using UnityEngine;

namespace droneSim.UI.Implementations.Unity.ExitUIElement
{
    public class DoorAnimatedComponent : MonoBehaviour, IUIAnimatedComponent
    {
        private Quaternion _initialLocalRotation;

        private void Awake()
        {
            _initialLocalRotation = transform.localRotation;
        }

        public void AnimateOnFocusGained()
        {
            ResetState();
            Action closeDoor = () =>
            {
                LeanTween.rotateLocal(gameObject, _initialLocalRotation.eulerAngles, 1f)
                    .setEaseOutExpo();
            };
            LeanTween.rotateLocal(gameObject, _initialLocalRotation.eulerAngles + new Vector3(0f, 0f, -100f), 1f)
                .setEaseOutExpo()
                .setOnComplete(closeDoor);
        }

        public void AnimateOnFocusLost()
        {
            ResetState();
        }

        public void ResetState()
        {
            LeanTween.cancel(gameObject);
            transform.localRotation = _initialLocalRotation;
        }
    }
}
