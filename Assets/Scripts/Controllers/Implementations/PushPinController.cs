﻿using droneSim.Controllers.Interfaces;
using System;
using UnityEngine;

namespace droneSim.Controllers.Implementations
{
    public class PushPinController : MonoBehaviour, IPushPinController
    {
        public event EventHandler<bool> OnPinStateChanged;

        private Vector3 _initialLocalPosition;

        private void Awake()
        {
            _initialLocalPosition = transform.localPosition;
            gameObject.SetActive(false);
        }

        public void TogglePinState(bool newState)
        {
            if (newState == true)
            {
                EnablePin();
            }
            else
            {
                DisablePin();
            }
        }

        private void EnablePin()
        {
            LeanTween.cancel(gameObject);
            gameObject.SetActive(true);
            LeanTween.moveLocal(gameObject, _initialLocalPosition + -Vector3.up * 0.1f, 0.5f)
                 .setEaseInQuint()
                 .setOnComplete(() => OnPinStateChanged?.Invoke(this, true));
        }

        private void DisablePin()
        {
            LeanTween.cancel(gameObject);
            LeanTween.moveLocal(gameObject, _initialLocalPosition, 0.5f)
                 .setEaseOutQuint()
                 .setOnComplete(() =>
                 {
                     gameObject.SetActive(false);
                     OnPinStateChanged?.Invoke(this, false);
                 });
        }
    }
}
