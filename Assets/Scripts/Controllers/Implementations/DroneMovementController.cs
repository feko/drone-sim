﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class DroneMovementController : MonoBehaviour, IDroneMovementController
    {
        public float Acceleration { get; private set; } = 0f;
        public float Rotation { get; private set; } = 0f;

        public Vector2 Tilt { get { return _tilt; } }

        private bool _initialized = false;
        private bool _shouldStraighten = false;
        private Vector2 _tilt = Vector2.zero;

        private Quaternion _previousRotation = Quaternion.identity;
        private Quaternion _currentRotation = Quaternion.identity;
        private float _rotationStrength = 0f;
        private Vector3 _initialPosition;
        private Quaternion _initialRotation;

        private ConstantForce _constantForce;
        private Rigidbody _rigidBody;

        private IDroneInputEventDispatcher _eventDispatcher;
        private DroneData _droneData;
        private IHintController _hintController;

        [Inject]
        private void Inject(IDroneInputEventDispatcher eventDispatcher,
            DroneData droneData,
            IHintController hintController)
        {
            _eventDispatcher = eventDispatcher;
            _droneData = droneData;
            _hintController = hintController;
        }

        private void Awake()
        {
            _constantForce = GetComponent<ConstantForce>();
            _rigidBody = GetComponent<Rigidbody>();
            _initialPosition = transform.localPosition;
            _initialRotation = transform.localRotation;
        }

        private void OnEnable()
        {
            if (!_initialized)
            {
                _eventDispatcher.OnAccelerationInputReceived += OnAccelerationInputReceived;
                _eventDispatcher.OnRotationInputReceived += OnRotationInputReceived;
                _eventDispatcher.OnTiltInputReceived += OnTiltInputReceived;

                _rigidBody.isKinematic = false;
                _hintController.ShowHint(Data.Models.HintType.FlyDrone, 3f);

                _initialized = true;
            }
        }

        private void OnDisable()
        {
            if (_initialized)
            {
                _eventDispatcher.OnAccelerationInputReceived -= OnAccelerationInputReceived;
                _eventDispatcher.OnRotationInputReceived -= OnRotationInputReceived;
                _eventDispatcher.OnTiltInputReceived -= OnTiltInputReceived;

                _rigidBody.isKinematic = true;

                Acceleration = 0f;
                Rotation = 0f;
                _tilt = Vector2.zero;

                _currentRotation = Quaternion.identity;
                _previousRotation = Quaternion.identity;
                _rotationStrength = 0f;

                transform.localPosition = _initialPosition;
                transform.localRotation = _initialRotation;
                _hintController.HideHints();

                _initialized = false;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            _shouldStraighten = false;
        }

        private void OnCollisionExit(Collision collision)
        {
            _shouldStraighten = true;
        }

        private void OnAccelerationInputReceived(object sender, float acceleration)
        {
            if (Mathf.Abs(acceleration) >= _droneData.AccelerationThreshold)
            {
                Acceleration = _droneData.AccelerationBase +
                    _droneData.AccelerationVariation * acceleration * _droneData.AccelerationModifier;
            }
            else
            {
                Acceleration = _droneData.AccelerationBase;
            }
        }

        private void OnRotationInputReceived(object sender, float rotation)
        {
            if (Mathf.Abs(rotation) >= _droneData.RotationThreshold)
            {
                Rotation += rotation * _droneData.RotationModifier * _droneData.RotationSpeed;
                _rotationStrength = _droneData.RotationSpeed * Mathf.Sign(rotation);
            }
            else
            {
                _rotationStrength = Mathf.Lerp(_rotationStrength, 0f, _droneData.RotationDrag);
                Rotation += _rotationStrength;
            }
        }

        private void OnTiltInputReceived(object sender, Vector2 tilt)
        {
            if (Mathf.Abs(tilt.x) >= _droneData.TiltThreshold.x)
            {
                _tilt.x = _droneData.TiltBase.x +
                    tilt.x * _droneData.TiltModifier.x * _droneData.TiltVariation.x;
            }
            else if (_tilt.x != 0f)
            {
                if (_tilt.x < 0)
                {
                    var newTiltX = _tilt.x + _droneData.TiltEaseStep.x;
                    var minTiltX = _droneData.TiltBase.x - _droneData.TiltVariation.x;
                    var maxTiltX = _droneData.TiltBase.x;

                    _tilt.x = Mathf.Clamp(newTiltX, minTiltX, maxTiltX);
                }
                else
                {
                    var newTiltX = _tilt.x - _droneData.TiltEaseStep.x;
                    var minTiltX = _droneData.TiltBase.x;
                    var maxTiltX = _droneData.TiltBase.x + _droneData.TiltVariation.x;
                    _tilt.x = Mathf.Clamp(newTiltX, minTiltX, maxTiltX);
                }
            }

            if (Mathf.Abs(tilt.y) >= _droneData.TiltThreshold.y)
            {
                _tilt.y = _droneData.TiltBase.y
                    + tilt.y * _droneData.TiltModifier.y * _droneData.TiltVariation.y;
            }
            else if (_tilt.y != 0f)
            {
                if (_tilt.y < 0)
                {
                    var newTiltY = _tilt.y + _droneData.TiltEaseStep.y;
                    var minTiltY = _droneData.TiltBase.y - _droneData.TiltVariation.y;
                    var maxTiltY = _droneData.TiltBase.y;
                    _tilt.y = Mathf.Clamp(newTiltY, minTiltY, maxTiltY);
                }
                else
                {
                    var newTiltY = _tilt.y - _droneData.TiltEaseStep.y;
                    var minTiltY = _droneData.TiltBase.y;
                    var maxTiltY = _droneData.TiltBase.y + _droneData.TiltVariation.y;
                    _tilt.y = Mathf.Clamp(newTiltY, minTiltY, maxTiltY);
                }
            }
        }

        private void Update()
        {
            if (_initialized)
            {
                _constantForce.relativeForce = new Vector3(0f, 0f, Acceleration);

                var tiltRotation = _droneData.MaximumTiltRotation;
                tiltRotation.y *= _tilt.x;
                tiltRotation.x *= _tilt.y;
                _currentRotation = Quaternion.Euler(-tiltRotation);
                var yRotation = Quaternion.Euler(Rotation * _droneData.RotationAxis);
                transform.localRotation *= Quaternion.Inverse(_previousRotation);
                transform.localRotation *= yRotation;
                transform.localRotation *= _currentRotation;
                _previousRotation = yRotation * _currentRotation;
                if (_shouldStraighten && Tilt == Vector2.zero)
                {
                    var straightRotation = Quaternion.Euler(_initialRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, straightRotation, Time.deltaTime * _droneData.RotationRecoverModifier);
                }
            }
        }
    }
}