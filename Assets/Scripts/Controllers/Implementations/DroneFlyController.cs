﻿using System;
using droneSim.Accessors;
using droneSim.Controllers.Interfaces;
using droneSim.Input.Interfaces;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations.Unity
{
    public class DroneFlyController : MonoBehaviour, IDroneFlyController
    {
        private MainCameraAccessor _mainCamera;
        private IDroneMovementController _droneMovementController;
        private IAudioController _audioController;
        private IUIInputEventDispatcher _uIInputEventDispatcher;
        private IApplicationController _applicationController;

        private Vector3 _initialScale;

        private Vector3 _basePosition;
        private Quaternion _baseRotation;
        private Vector3 _baseScale = Vector3.one * 0.25f;

        [Inject]
        private void Inject(MainCameraAccessor mainCameraAccessor,
            IDroneMovementController droneMovementController,
            IAudioController audioController,
            IUIInputEventDispatcher uIInputEventDispatcher,
            IApplicationController applicationController)
        {
            _mainCamera = mainCameraAccessor;
            _droneMovementController = droneMovementController;
            _audioController = audioController;
            _uIInputEventDispatcher = uIInputEventDispatcher;
            _applicationController = applicationController;
        }

        private void Awake()
        {
            _initialScale = transform.localScale;
            gameObject.SetActive(false);
        }

        private void OnBack(object sender, EventArgs e)
        {
            _uIInputEventDispatcher.OnBack -= OnBack;

            StopFlying();
            _applicationController.ShowMainMenu();
        }

        public void StopFlying()
        {
            LeanTween.cancel(gameObject);
            gameObject.SetActive(false);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            ((MonoBehaviour)_droneMovementController).enabled = false;
        }

        public void StartFlying(Transform origin)
        {
            LeanTween.cancel(gameObject);
            _audioController.SetGlobalVolume(0);
            gameObject.SetActive(true);
            transform.position = _basePosition = origin.position;
            transform.rotation = _baseRotation = origin.rotation;
            transform.localScale = _baseScale;
            var mainCameraEulerRotation = _mainCamera.transform.rotation.eulerAngles;
            LeanTween.value(0f, 1f, 1f).setOnUpdate((value) =>
            {
                var newPosition = _mainCamera.transform.position + _mainCamera.transform.forward * value * 1.5f;
                transform.position = Vector3.Lerp(_basePosition, newPosition, value);
                transform.rotation = Quaternion.Lerp(_baseRotation, Quaternion.Euler(0f, mainCameraEulerRotation.y, 0f), value);
                transform.localScale = Vector3.Lerp(_baseScale, _initialScale, value);
                _audioController.SetGlobalVolume(value);
            }).setOnComplete(() =>
            {
                ((MonoBehaviour)_droneMovementController).enabled = true;
                _uIInputEventDispatcher.OnBack += OnBack;
            });
        }
    }
}
