﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using System;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class ApplicationController : MonoBehaviour, IApplicationController
    {
        public ApplicationState ApplicationState { get; private set; } = ApplicationState.Idle;

        private IUIController _uIController;
        private IUIInputEventDispatcher _inputEventDispatcher;
        private IDroneFlyController _droneFlyController;
        private IObstaclePlacingController _obstaclePlacingController;

        [Inject]
        private void Inject(IUIController uIController,
            IUIInputEventDispatcher inputEventDispatcher,
            IDroneFlyController droneFlyController,
            IObstaclePlacingController obstaclePlacingController)
        {
            _uIController = uIController;
            _inputEventDispatcher = inputEventDispatcher;
            _droneFlyController = droneFlyController;
            _obstaclePlacingController = obstaclePlacingController;
        }

        private void Start()
        {
            _uIController.Show(UIMenu.MainMenu);
            ApplicationState = ApplicationState.Menu;
        }

        private void OnEnable()
        {
            _inputEventDispatcher.OnPause += OnPause;
        }

        private void OnDisable()
        {
            _inputEventDispatcher.OnPause -= OnPause;
        }

        public void StartFlyingDrone(Transform droneOrigin)
        {
            _uIController.Hide();
            _droneFlyController.StartFlying(droneOrigin);
            ApplicationState = ApplicationState.FlyingDrone;
        }

        public void StartPlacingObstacles(ObstacleType obstacleType)
        {
            _uIController.Hide();
            _obstaclePlacingController.StartPlacingObstacle(obstacleType);
            ApplicationState = ApplicationState.PlacingObstacles;
        }

        public void StartDeletingObstacles()
        {
            _uIController.Hide();
            _obstaclePlacingController.StartDeletingObstacle();
            ApplicationState = ApplicationState.PlacingObstacles;
        }

        public void ShowMainMenu()
        {
            _uIController.Show(UIMenu.MainMenu);
            ApplicationState = ApplicationState.Menu;
        }

        public void ShowObstacleMenu()
        {
            _uIController.Show(UIMenu.Obstacles);
            ApplicationState = ApplicationState.Menu;
        }

        private void OnPause(object sender, EventArgs e)
        {
            if (LeanTween.tweensRunning == 0)
            {
                switch (ApplicationState)
                {
                    case ApplicationState.Idle:
                        _uIController.Show(UIMenu.MainMenu);
                        ApplicationState = ApplicationState.Menu;
                        break;
                    case ApplicationState.Menu:
                        _uIController.Hide();
                        ApplicationState = ApplicationState.Idle;
                        break;
                    case ApplicationState.FlyingDrone:
                        _droneFlyController.StopFlying();
                        _uIController.Show(UIMenu.MainMenu);
                        ApplicationState = ApplicationState.Menu;
                        break;
                    case ApplicationState.PlacingObstacles:
                        _uIController.Show(UIMenu.Obstacles);
                        ApplicationState = ApplicationState.Menu;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
