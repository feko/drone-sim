﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    [RequireComponent(typeof(AudioSource))]
    public class DroneSoundController : MonoBehaviour
    {
        private AudioSource _droneSound;
        private float _currentPitch = 0f;

        private IDroneMovementController _droneMovementController;
        private DroneData _droneData;

        [Inject]
        private void Inject(IDroneMovementController droneMovementController, DroneData droneData)
        {
            _droneMovementController = droneMovementController;
            _droneData = droneData;
        }

        private void Awake()
        {
            _droneSound = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            if (!_droneSound.isPlaying)
            {
                _droneSound.Play();
            }
        }

        private void Update()
        {
            var pitchRatio = _droneData.AccelerationVariation * 2 / 0.1f;
            _currentPitch = 0.95f + (_droneMovementController.Acceleration - _droneData.AccelerationVariation) / pitchRatio;
            _droneSound.pitch = _currentPitch;
        }

        private void OnDisable()
        {
            if (_droneSound.isPlaying)
            {
                _droneSound.Stop();
            }
        }
    }
}