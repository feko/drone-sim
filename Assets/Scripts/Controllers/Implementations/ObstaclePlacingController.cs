﻿using droneSim.Accessors;
using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using droneSim.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class ObstaclePlacingController : MonoBehaviour, IObstaclePlacingController
    {
        private ObstacleData _obstacleData;
        private Transform _mainCamera;
        private IUIInputEventDispatcher _uIInputEventDispatcher;
        private IApplicationController _applicationController;
        private IFocusController _focusController;
        private IObstacleInputProvider _obstacleInputProvider;
        private IAudioController _audioController;
        private IHintController _hintController;

        private GameObject _obstacleBeingPlaced;
        private readonly List<GameObject> _placedObstacles = new List<GameObject>();

        private bool IsPlacing { get { return _obstacleBeingPlaced != null; } }

        public bool ObstaclesPlaced => _placedObstacles.Count != 0;

        [Inject]
        private void Inject(ObstacleData obstacleData,
            MainCameraAccessor mainCameraAccessor,
            IUIInputEventDispatcher uIInputEventDispatcher,
            IApplicationController applicationController,
            IFocusController focusController,
            IObstacleInputProvider obstacleInputProvider,
            IAudioController audioController,
            IHintController hintController)
        {
            _obstacleData = obstacleData;
            _mainCamera = mainCameraAccessor.transform;
            _uIInputEventDispatcher = uIInputEventDispatcher;
            _applicationController = applicationController;
            _focusController = focusController;
            _obstacleInputProvider = obstacleInputProvider;
            _audioController = audioController;
            _hintController = hintController;
        }

        private void Update()
        {
            if (IsPlacing)
            {
                var positionOffset = Vector3.ProjectOnPlane(_mainCamera.forward, Vector3.up).normalized * _obstacleInputProvider.ForwardBackwardAxis;
                positionOffset += Vector3.ProjectOnPlane(_mainCamera.right, Vector3.up).normalized * _obstacleInputProvider.LeftRightAxis;
                positionOffset += Vector3.up * _obstacleInputProvider.UpDownAxis;
                positionOffset *= Time.deltaTime;

                var rotationOffset = Quaternion.AngleAxis(-_obstacleInputProvider.Rotation, Vector3.up);

                var scaleOffset = _obstacleInputProvider.ScaleAxis * Time.deltaTime * Vector3.one;
                var newScale = _obstacleBeingPlaced.transform.localScale + scaleOffset;
                newScale = newScale.Clamp(Vector3.one * 0.1f, Vector3.one * float.MaxValue);

                _obstacleBeingPlaced.transform.position += positionOffset;
                _obstacleBeingPlaced.transform.rotation *= rotationOffset;
                _obstacleBeingPlaced.transform.localScale = newScale;
            }
        }

        public void StartPlacingObstacle(ObstacleType obstacleType)
        {
            var obstacle = _obstacleData.Obstacles.FirstOrDefault(o => o.ObstacleType == obstacleType);
            Debug.Assert(obstacle != null);

            _obstacleBeingPlaced = Instantiate(obstacle.ObstaclePrefab, transform);
            _obstacleBeingPlaced.transform.position = _mainCamera.transform.position + _mainCamera.transform.forward * 1.5f;

            _uIInputEventDispatcher.OnBack += StopPlacing;
            _uIInputEventDispatcher.OnPause += StopPlacing;
            _uIInputEventDispatcher.OnConfirm += ConfirmPlacement;

            _hintController.ShowHint(HintType.ObstaclePlacing, 3f);
        }

        public void StartDeletingObstacle()
        {
            _focusController.UpdateFocusableObjects(_placedObstacles.Select(po => po.GetComponent<IFocusable>()));

            _uIInputEventDispatcher.OnBack += StopDeleting;
            _uIInputEventDispatcher.OnPause += StopDeleting;
            _uIInputEventDispatcher.OnConfirm += ConfirmDeletion;
        }

        private void StopPlacing(object sender, EventArgs e)
        {
            _uIInputEventDispatcher.OnBack -= StopPlacing;
            _uIInputEventDispatcher.OnPause -= StopPlacing;
            _uIInputEventDispatcher.OnConfirm -= ConfirmPlacement;

            Destroy(_obstacleBeingPlaced);
            _obstacleBeingPlaced = null;
            _applicationController.ShowObstacleMenu();
        }

        private void ConfirmPlacement(object sender, EventArgs e)
        {
            _audioController.PlaySound(SoundType.Place);

            _uIInputEventDispatcher.OnBack -= StopPlacing;
            _uIInputEventDispatcher.OnPause -= StopPlacing;
            _uIInputEventDispatcher.OnConfirm -= ConfirmPlacement;

            LeanTween.cancel(_obstacleBeingPlaced);
            var originalScale = _obstacleBeingPlaced.transform.localScale;
            _obstacleBeingPlaced.transform.localScale *= 0.75f;
            LeanTween.scale(_obstacleBeingPlaced, originalScale, 0.25f)
                .setEaseOutElastic()
                .setOnComplete(() =>
                {
                    _placedObstacles.Add(_obstacleBeingPlaced);
                    _obstacleBeingPlaced = null;
                    _applicationController.ShowObstacleMenu();
                });
        }

        private void StopDeleting(object sender, EventArgs e)
        {
            _focusController.UpdateFocusableObjects(Enumerable.Empty<IFocusable>());

            _uIInputEventDispatcher.OnBack -= StopDeleting;
            _uIInputEventDispatcher.OnPause -= StopDeleting;
            _uIInputEventDispatcher.OnConfirm -= ConfirmDeletion;

            _applicationController.ShowObstacleMenu();
        }

        private void ConfirmDeletion(object sender, EventArgs e)
        {
            _audioController.PlaySound(SoundType.Delete);
            var objectToDelete = ((MonoBehaviour)_focusController.FocusedObject).gameObject;
            _placedObstacles.Remove(objectToDelete);
            LeanTween.cancel(objectToDelete);
            LeanTween.scale(objectToDelete, Vector3.zero, 0.25f)
                .setEaseOutQuint()
                .setOnComplete(() =>
                {
                    Destroy(objectToDelete);
                    _focusController.UpdateFocusableObjects(_placedObstacles.Select(po => po.GetComponent<IFocusable>()));
                    if (_placedObstacles.Count == 0)
                    {
                        _uIInputEventDispatcher.OnBack -= StopDeleting;
                        _uIInputEventDispatcher.OnPause -= StopDeleting;
                        _uIInputEventDispatcher.OnConfirm -= ConfirmDeletion;

                        _applicationController.ShowObstacleMenu();
                    }
                });

        }
    }
}
