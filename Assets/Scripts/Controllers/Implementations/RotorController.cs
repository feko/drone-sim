﻿using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class RotorController : MonoBehaviour
    {
        public Vector3 MaxRotation;
        public RotorSide RotorSide;

        private Quaternion _initialRotation;

        private IDroneInputProvider _inputProvider;

        [Inject]
        private void Inject(IDroneInputProvider inputProvider)
        {
            _inputProvider = inputProvider;
        }

        private void Awake()
        {
            _initialRotation = transform.localRotation;
        }

        private void Update()
        {
            var rotationInput = _inputProvider.RotationInput;
            var rotationToApply = Quaternion.Euler(MaxRotation * rotationInput);
            if (RotorSide == RotorSide.Right)
            {
                rotationToApply = Quaternion.Inverse(rotationToApply);
            }
            transform.localRotation = _initialRotation * rotationToApply;
        }
    }
}
