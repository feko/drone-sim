﻿using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioController : MonoBehaviour, IAudioController
    {
        private AudioSource _globalAudioSource;

        private SoundData _soundData;
        private readonly List<AudioSource> _audioSources = new List<AudioSource>();
        private readonly Dictionary<AudioSource, float> _initialVolumes = new Dictionary<AudioSource, float>();

        [Inject]
        private void Inject(List<AudioSource> audioSources, SoundData soundData)
        {
            _soundData = soundData;
            _globalAudioSource = GetComponent<AudioSource>();
            _audioSources.AddRange(audioSources);
            _audioSources.ForEach(@as => _initialVolumes.Add(@as, @as.volume));
        }

        public void SetGlobalVolume(float volume)
        {
            volume = Mathf.Clamp(volume, 0f, 1f);
            _audioSources.ForEach(@as => @as.volume = volume * _initialVolumes[@as]);
        }

        public void PlaySound(SoundType soundType)
        {
            var sound = _soundData.Sounds.FirstOrDefault(s => s.SoundType == soundType)?.SoundClip;
            if (sound != null)
            {
                _globalAudioSource.clip = sound;
                _globalAudioSource.Play();
            }
        }
    }
}
