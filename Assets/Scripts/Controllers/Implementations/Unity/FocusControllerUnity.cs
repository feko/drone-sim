﻿using droneSim.Controllers.Interfaces;
using droneSim.Input.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations.Unity
{
    public class FocusControllerUnity : MonoBehaviour, IFocusController
    {
        public IFocusable FocusedObject => _focusableObjects.ElementAtOrDefault(CurrentIndex);

        private IUIInputEventDispatcher _uIInputEventDispatcher;

        private readonly List<IFocusable> _focusableObjects = new List<IFocusable>();
        private int _currentIndex = 0;

        private int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                var nObjects = _focusableObjects.Count;
                if (nObjects == 0) return;
                _currentIndex = (value % nObjects + nObjects) % nObjects;
            }
        }

        [Inject]
        private void Inject(IUIInputEventDispatcher eventDispatcher)
        {
            _uIInputEventDispatcher = eventDispatcher;
        }

        private void OnEnable()
        {
            _uIInputEventDispatcher.OnLeft += FocusPreviousObject;
            _uIInputEventDispatcher.OnRight += FocusNextObject;
        }

        private void OnDisable()
        {
            _uIInputEventDispatcher.OnLeft -= FocusPreviousObject;
            _uIInputEventDispatcher.OnRight -= FocusNextObject;
        }

        public void UpdateFocusableObjects(IEnumerable<IFocusable> focusableObjects)
        {
            FocusedObject?.OnFocusLost();
            _focusableObjects.Clear();
            _focusableObjects.AddRange(focusableObjects);
            CurrentIndex = 0;
            FocusedObject?.OnFocusGained();
        }

        private void FocusNextObject(object sender, EventArgs e)
        {
            var previousFocusedObject = FocusedObject;
            ++CurrentIndex;
            if (FocusedObject != previousFocusedObject)
            {
                previousFocusedObject?.OnFocusLost();
                FocusedObject?.OnFocusGained();
            }
        }

        private void FocusPreviousObject(object sender, EventArgs e)
        {
            var previousFocusedObject = FocusedObject;
            --CurrentIndex;
            if (FocusedObject != previousFocusedObject)
            {
                previousFocusedObject?.OnFocusLost();
                FocusedObject?.OnFocusGained();
            }
        }
    }
}
