﻿using droneSim.Data.Models;
using UnityEditor;
using UnityEngine;

namespace droneSim.Controllers.Implementations.Unity.Editor
{
    [CustomEditor(typeof(UIControllerUnity))]
    public class UIControllerUnityEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var script = target as UIControllerUnity;
            if (GUILayout.Button("Show Main Menu"))
            {
                script.Show(UIMenu.MainMenu);
            }
            if (GUILayout.Button("Show Obstacles"))
            {
                script.Show(UIMenu.Obstacles);
            }
            if (GUILayout.Button("Show None"))
            {
                script.Show(UIMenu.None);
            }
            if (GUILayout.Button("Hide"))
            {
                script.Hide();
            }
        }
    }
}
