﻿using droneSim.Accessors;
using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using droneSim.Input.Interfaces;
using droneSim.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations.Unity
{
    public class UIControllerUnity : MonoBehaviour, IUIController
    {

        public float UIDistance { get; set; } = 2f;
        private readonly float _lerpSpeed = 2f;

        private readonly List<IUIMenu> _uIMenuElements = new List<IUIMenu>();
        private IFocusController _focusController;
        private IUIInputEventDispatcher _uIInputEventController;
        private IPushPinController _pushPinController;
        private IHintController _hintController;

        private Transform _mainCamera;
        private bool _shouldFollow = true;
        private bool _previousFollowState = true;
        private Vector3 _previousPosition;
        private UIMenu _uIMenuToChange;

        private IUIMenu _currentMenu;
        private IUIMenu CurrentMenu
        {
            get { return _currentMenu; }
            set { _currentMenu = value; }
        }

        [Inject]
        private void Inject(List<IUIMenu> uIMenuElements,
            IFocusController focusController,
            IUIInputEventDispatcher inputEventDispatcher,
            MainCameraAccessor mainCameraAccessor,
            IPushPinController pushPinController,
            IHintController hintController)
        {
            _uIMenuElements.AddRange(uIMenuElements);
            _focusController = focusController;
            _uIInputEventController = inputEventDispatcher;
            _mainCamera = mainCameraAccessor.transform;
            _pushPinController = pushPinController;
            _hintController = hintController;
        }

        private void OnEnable()
        {
            _uIInputEventController.OnConfirm += OnElementSelected;
            _uIInputEventController.OnBack += OnBackPressed;
            _uIInputEventController.OnPin += OnPinPressed;

            _pushPinController.OnPinStateChanged += OnPinStateChanged;
        }

        private void Update()
        {
            if (_shouldFollow)
            {
                var newRotation = Quaternion.LookRotation(_mainCamera.transform.position - transform.position);
                newRotation = Quaternion.Euler(newRotation.eulerAngles.y * Vector3.up);
                transform.rotation = newRotation;

                var desiredPosition = _mainCamera.position + _mainCamera.forward * UIDistance;
                RaycastHit raycastHit;
                if (Physics.Raycast(_mainCamera.position, _mainCamera.forward, out raycastHit, UIDistance))
                {
                    desiredPosition = raycastHit.point + _mainCamera.forward * -0.2f;
                }
                transform.position = Vector3.Lerp(transform.position, desiredPosition, _lerpSpeed * Time.deltaTime);
            }
        }

        private void OnDisable()
        {
            _uIInputEventController.OnConfirm -= OnElementSelected;
            _uIInputEventController.OnBack -= OnBackPressed;
            _uIInputEventController.OnPin -= OnPinPressed;

            _pushPinController.OnPinStateChanged -= OnPinStateChanged;
        }

        private void OnElementSelected(object sender, EventArgs e)
        {
            var focusedElement = _focusController.FocusedObject as IUIMenuElement;
            focusedElement?.OnSelect();
        }

        private void OnBackPressed(object sender, EventArgs e)
        {
            Show(CurrentMenu?.ParentMenu ?? UIMenu.None);
        }

        private void OnPinPressed(object sender, EventArgs e)
        {
            if ((CurrentMenu?.UIMenu ?? UIMenu.None) != UIMenu.None)
            {
                _pushPinController.TogglePinState(_shouldFollow);
            }
        }

        private void OnPinStateChanged(object sender, bool newState)
        {
            _shouldFollow = !newState;
        }

        public void Hide()
        {
            if (CurrentMenu != null)
            {
                CurrentMenu.OnMenuHidden -= ShowMenuEventHandler;
                CurrentMenu.Hide();
            }

            _previousFollowState = _shouldFollow;
            _previousPosition = transform.position;
            ((MonoBehaviour)_pushPinController).gameObject.SetActive(false);
            _shouldFollow = true;

            _hintController.HideHints();
            CurrentMenu = null;
        }

        public void Show(UIMenu uIMenu)
        {
            if (CurrentMenu?.UIMenu != uIMenu)
            {
                _uIMenuToChange = uIMenu;
                if (CurrentMenu != null)
                {
                    _previousFollowState = _shouldFollow;
                    _previousPosition = transform.position;
                    ((MonoBehaviour)_pushPinController).gameObject.SetActive(false);
                    _shouldFollow = true;
                    CurrentMenu.OnMenuHidden += ShowMenuEventHandler;
                    CurrentMenu.Hide();
                }
                else
                {
                    ShowMenuEventHandler(this, EventArgs.Empty);
                }
            }
        }

        private void ShowMenuEventHandler(object sender, EventArgs args)
        {
            if (!_previousFollowState)
            {
                _pushPinController.TogglePinState(true);
                _shouldFollow = false;
                transform.position = _previousPosition;

                var newRotation = Quaternion.LookRotation(_mainCamera.transform.position - transform.position);
                newRotation = Quaternion.Euler(newRotation.eulerAngles.y * Vector3.up);
                transform.rotation = newRotation;
            }
            if (CurrentMenu != null) CurrentMenu.OnMenuHidden -= ShowMenuEventHandler;
            CurrentMenu = _uIMenuElements.FirstOrDefault(m => m.UIMenu == _uIMenuToChange);
            if (CurrentMenu != null)
            {
                CurrentMenu.Show();
                _hintController.ShowHint(HintType.General);
            }
            else
            {
                Hide();
            }
        }
    }
}
