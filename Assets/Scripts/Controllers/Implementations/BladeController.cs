﻿using droneSim.Controllers.Interfaces;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class BladeController : MonoBehaviour
    {
        public float RotationModifier;
        public Vector3 RotationAxis;

        private IDroneMovementController _droneController;

        [Inject]
        private void Inject(IDroneMovementController droneController)
        {
            _droneController = droneController;
        }

        void Update()
        {
            var rotation = _droneController.Acceleration * RotationModifier * Time.deltaTime * RotationAxis;
            transform.localRotation *= Quaternion.Euler(rotation);
        }
    }
}