﻿using droneSim.Accessors;
using droneSim.Controllers.Interfaces;
using droneSim.Data.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace droneSim.Controllers.Implementations
{
    public class HintController : MonoBehaviour, IHintController
    {
        private readonly List<HintAccessor> _hints = new List<HintAccessor>();
        private HintAccessor _currentHintShown;

        [Inject]
        private void Inject(List<HintAccessor> hintAccessors)
        {
            _hints.Clear();
            _hints.AddRange(hintAccessors);
            foreach (var hint in _hints)
            {
                hint.gameObject.SetActive(false);
            }
        }

        public void HideHints()
        {
            gameObject.SetActive(false);
        }

        public void ShowHint(HintType hintType, float hideAfter)
        {
            _currentHintShown?.gameObject.SetActive(false);
            _currentHintShown = _hints.FirstOrDefault(h => h.HintType == hintType);
            _currentHintShown?.gameObject.SetActive(true);
            gameObject.SetActive(true);

            StopAllCoroutines();
            if (hideAfter != 0f)
            {
                StartCoroutine(HideHintsCoroutine(hideAfter));
            }
        }

        private IEnumerator HideHintsCoroutine(float hideAfter)
        {
            yield return new WaitForSecondsRealtime(hideAfter);
            HideHints();
        }
    }
}
