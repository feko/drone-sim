﻿using droneSim.Data.Models;

namespace droneSim.Controllers.Interfaces
{
    public interface IHintController
    {
        void ShowHint(HintType hintType, float hideAfter = 0f);
        void HideHints();
    }
}