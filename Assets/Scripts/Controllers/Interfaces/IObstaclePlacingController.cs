﻿using droneSim.Data.Models;

namespace droneSim.Controllers.Interfaces
{
    public interface IObstaclePlacingController
    {
        bool ObstaclesPlaced { get; }
        void StartPlacingObstacle(ObstacleType obstacleType);
        void StartDeletingObstacle();
    }
}
