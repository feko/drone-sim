﻿using UnityEngine;

namespace droneSim.Controllers.Interfaces
{
    public interface IDroneFlyController 
    {
        void StartFlying(Transform origin);
        void StopFlying();
    } 
}
