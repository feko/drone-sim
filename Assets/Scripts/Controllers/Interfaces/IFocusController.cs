﻿using droneSim.Input.Interfaces;
using System.Collections.Generic;

namespace droneSim.Controllers.Interfaces
{
    public interface IFocusController
    {
        IFocusable FocusedObject { get; }
        void UpdateFocusableObjects(IEnumerable<IFocusable> focusableObjects);
    }
}
