﻿using droneSim.Data.Models;

namespace droneSim.Controllers.Interfaces
{
    public interface IAudioController
    {
        void SetGlobalVolume(float volume);
        void PlaySound(SoundType soundType);
    }
}
