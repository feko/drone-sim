﻿using droneSim.Data.Models;

namespace droneSim.Controllers.Interfaces
{
    public interface IUIController
    {
        float UIDistance { get; set; }

        void Show(UIMenu uIMenu);
        void Hide();
    }
}