﻿using UnityEngine;

namespace droneSim.Controllers.Interfaces
{
    public interface IDroneMovementController
    {
        float Acceleration { get; }
        float Rotation { get; }
        Vector2 Tilt { get; }
    }
}
