﻿using System;

namespace droneSim.Controllers.Interfaces
{
    public interface IPushPinController
    {
        event EventHandler<bool> OnPinStateChanged;
        void TogglePinState(bool newState);
    }
}
