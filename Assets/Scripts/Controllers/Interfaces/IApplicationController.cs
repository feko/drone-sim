﻿using droneSim.Data.Models;
using UnityEngine;

namespace droneSim.Controllers.Interfaces
{
    public interface IApplicationController
    {
        ApplicationState ApplicationState { get; }
        void StartPlacingObstacles(ObstacleType obstacleType);
        void StartDeletingObstacles();
        void ShowMainMenu();
        void ShowObstacleMenu();
        void StartFlyingDrone(Transform droneOrigin);
    }
}
