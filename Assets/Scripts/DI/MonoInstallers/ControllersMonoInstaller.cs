using droneSim.Controllers.Interfaces;
using Zenject;

namespace droneSim.DI.MonoInstallers
{
    public class ControllersMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IDroneMovementController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IFocusController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IUIController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IDroneFlyController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IAudioController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IApplicationController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IObstaclePlacingController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IPushPinController>().FromComponentInChildren().AsSingle();
            Container.Bind<IHintController>().FromComponentInHierarchy().AsSingle();
        }
    }
}