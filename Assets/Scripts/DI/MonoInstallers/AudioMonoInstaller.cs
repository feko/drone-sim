using UnityEngine;
using Zenject;

namespace droneSim.DI.MonoInstallers
{
    public class AudioMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<AudioSource>().FromComponentsInHierarchy().AsSingle();
        }
    } 
}