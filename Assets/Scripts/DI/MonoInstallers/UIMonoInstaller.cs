using droneSim.UI.Interfaces;
using Zenject;

namespace droneSim.DI.MonoInstallers
{
    public class UIMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IUIMenu>().FromComponentsInChildren();
            Container.Bind<IUIMenuElement>().FromComponentsInChildren();
            Container.Bind<IUIAnimatedComponent>().FromComponentsInChildren();
        }
    }
}