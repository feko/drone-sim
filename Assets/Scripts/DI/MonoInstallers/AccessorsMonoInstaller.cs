using droneSim.Accessors;
using Zenject;

namespace droneSim.DI.MonoInstallers
{
    public class AccessorsMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<MainCameraAccessor>().FromComponentInHierarchy().AsSingle();
            Container.Bind<HintAccessor>().FromComponentsInChildren().AsSingle();
        }
    } 
}