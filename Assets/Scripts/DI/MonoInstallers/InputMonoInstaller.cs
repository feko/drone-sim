using droneSim.Input.Interfaces;
using Zenject;

namespace droneSim.DI.MonoInstallers
{
    public class InputMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IDroneInputProvider>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IDroneInputEventDispatcher>().FromComponentInHierarchy().AsSingle();

            Container.Bind<IUIInputProvider>().FromComponentInHierarchy().AsSingle();
            Container.Bind<IUIInputEventDispatcher>().FromComponentInHierarchy().AsSingle();

            Container.Bind<IObstacleInputProvider>().FromComponentInHierarchy().AsSingle();
        }
    }
}