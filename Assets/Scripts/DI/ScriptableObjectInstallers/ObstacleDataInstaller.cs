using droneSim.Data.Models;
using UnityEngine;
using Zenject;

namespace droneSim.DI.ScriptableObjectInstallers
{
    [CreateAssetMenu(fileName = "ObstacleDataInstaller", menuName = "Installers/ObstacleDataInstaller")]
    public class ObstacleDataInstaller : ScriptableObjectInstaller<ObstacleDataInstaller>
    {
        public ObstacleData ObstacleData;

        public override void InstallBindings()
        {
            Container.BindInstance(ObstacleData);
        }
    }
}