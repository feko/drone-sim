using droneSim.Data.Models;
using UnityEngine;
using Zenject;

namespace droneSim.DI.ScriptableObjectInstallers
{
    [CreateAssetMenu(fileName = "DroneDataInstaller", menuName = "Installers/DroneDataInstaller")]
    public class DroneDataInstaller : ScriptableObjectInstaller<DroneDataInstaller>
    {
        public DroneData DroneData;

        public override void InstallBindings()
        {
            Container.BindInstance(DroneData);
        }
    } 
}