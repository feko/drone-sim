using droneSim.Data.Models;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "SoundDataInstaller", menuName = "Installers/SoundDataInstaller")]
public class SoundDataInstaller : ScriptableObjectInstaller<SoundDataInstaller>
{
    public SoundData SoundData;

    public override void InstallBindings()
    {
        Container.BindInstance(SoundData);
    }
}