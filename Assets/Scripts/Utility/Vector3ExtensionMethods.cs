﻿using UnityEngine;

namespace droneSim.Utility
{
    public static class Vector3ExtensionMethods
    {
        public static Vector3 Clamp(this Vector3 original, Vector3 minimum, Vector3 maximum)
        {
            var clampedX = Mathf.Clamp(original.x, minimum.x, maximum.x);
            var clampedY = Mathf.Clamp(original.y, minimum.y, maximum.y);
            var clampedZ = Mathf.Clamp(original.z, minimum.z, maximum.z);
            return new Vector3(clampedX, clampedY, clampedZ);
        }
    }
}
