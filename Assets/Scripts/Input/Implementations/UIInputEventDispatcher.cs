﻿using droneSim.Input.Interfaces;
using System;
using UnityEngine;
using Zenject;

namespace droneSim.Input.Implementations
{
    public class UIInputEventDispatcher : MonoBehaviour, IUIInputEventDispatcher
    {
        [SerializeField]
        private bool _debugTextOn = false;

        private IUIInputProvider _uiInputProvider;

        public virtual event EventHandler OnConfirm;
        public virtual event EventHandler OnBack;
        public virtual event EventHandler OnLeft;
        public virtual event EventHandler OnRight;
        public virtual event EventHandler OnPause;
        public virtual event EventHandler OnPin;

        private bool _confirmActive;
        private bool _backActive;
        private bool _leftActive;
        private bool _rightActive;
        private bool _pauseActive;
        private bool _pinActive;

        [Inject]
        private void Inject(IUIInputProvider uiInputProvider)
        {
            _uiInputProvider = uiInputProvider;
        }

        private void Update()
        {
            if (_uiInputProvider.ConfirmActive && !_confirmActive)
            {
                if (_debugTextOn) Debug.Log("Confirm action activate.");
                OnConfirm?.Invoke(this, EventArgs.Empty);
            }
            _confirmActive = _uiInputProvider.ConfirmActive;

            if (_uiInputProvider.BackActive && !_backActive)
            {
                if (_debugTextOn) Debug.Log("Back action activate.");
                OnBack?.Invoke(this, EventArgs.Empty);
            }
            _backActive = _uiInputProvider.BackActive;

            if (_uiInputProvider.LeftActive && !_leftActive)
            {
                if (_debugTextOn) Debug.Log("Left action activate.");
                OnLeft?.Invoke(this, EventArgs.Empty);
            }
            _leftActive = _uiInputProvider.LeftActive;

            if (_uiInputProvider.RightActive && !_rightActive)
            {
                if (_debugTextOn) Debug.Log("Right action activate.");
                OnRight?.Invoke(this, EventArgs.Empty);
            }
            _rightActive = _uiInputProvider.RightActive;

            if (_uiInputProvider.PauseActive && !_pauseActive)
            {
                if (_debugTextOn) Debug.Log("Pause action activate.");
                OnPause?.Invoke(this, EventArgs.Empty);
            }
            _pauseActive = _uiInputProvider.PauseActive;

            if (_uiInputProvider.PinActive && !_pinActive)
            {
                if (_debugTextOn) Debug.Log("Pin action activate.");
                OnPin?.Invoke(this, EventArgs.Empty);
            }
            _pinActive = _uiInputProvider.PinActive;
        }
    }
}
