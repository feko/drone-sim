﻿using droneSim.Input.Interfaces;
using UnityEngine;

namespace droneSim.Input.Implementations.Unity
{
    public class ObstacleInputProviderUnity : MonoBehaviour, IObstacleInputProvider
    {
        public float ForwardBackwardAxis => UnityEngine.Input.GetAxis("Gamepad Right Y");

        public float LeftRightAxis => UnityEngine.Input.GetAxis("Gamepad Right X");

        public float UpDownAxis => UnityEngine.Input.GetAxis("Gamepad Left Y");

        public float Rotation => UnityEngine.Input.GetAxis("Gamepad Left X");

        public float ScaleAxis => UnityEngine.Input.GetAxis("Triggers");
    }
}
