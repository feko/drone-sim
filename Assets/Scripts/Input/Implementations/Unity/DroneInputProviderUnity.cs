﻿using droneSim.Input.Interfaces;
using UnityEngine;

namespace droneSim.Input.Implementations.Unity
{
    public class DroneInputProviderUnity : MonoBehaviour, IDroneInputProvider
    {
        public bool ControllerConnected => UnityEngine.Input.GetJoystickNames().Length > 0;
        public float AccelerationInput => UnityEngine.Input.GetAxis("Gamepad Left Y");
        public float RotationInput => UnityEngine.Input.GetAxis("Gamepad Left X");
        public float TiltInputX => UnityEngine.Input.GetAxis("Gamepad Right X");
        public float TiltInputY => UnityEngine.Input.GetAxis("Gamepad Right Y");
    }
}
