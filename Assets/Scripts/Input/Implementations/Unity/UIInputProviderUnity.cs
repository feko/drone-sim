﻿using droneSim.Input.Interfaces;
using UnityEngine;

namespace droneSim.Input.Implementations.Unity
{
    public class UIInputProviderUnity : MonoBehaviour, IUIInputProvider
    {
        public bool ConfirmActive => UnityEngine.Input.GetButtonDown("Confirm button");

        public bool BackActive => UnityEngine.Input.GetButtonDown("Back button");

        public bool LeftActive => UnityEngine.Input.GetAxis("D-pad axis") < -0.5f;

        public bool RightActive => UnityEngine.Input.GetAxis("D-pad axis") > 0.5f;

        public bool PauseActive => UnityEngine.Input.GetButtonDown("Start button");

        public bool PinActive => UnityEngine.Input.GetButtonDown("Pin button");
    }

}