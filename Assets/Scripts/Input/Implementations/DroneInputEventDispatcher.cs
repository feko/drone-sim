﻿using droneSim.Input.Interfaces;
using System;
using UnityEngine;
using Zenject;

namespace droneSim.Input.Implementations
{
    public class DroneInputEventDispatcher : MonoBehaviour, IDroneInputEventDispatcher
    {
        [SerializeField]
        private bool _debugTextOn = false;

        private IDroneInputProvider _droneInputProvider;

        public virtual event EventHandler<float> OnAccelerationInputReceived;
        public virtual event EventHandler<Vector2> OnTiltInputReceived;
        public virtual event EventHandler<float> OnRotationInputReceived;

        [Inject]
        private void Inject(IDroneInputProvider droneInputProvider)
        {
            _droneInputProvider = droneInputProvider;
        }

        void Update()
        {
            var accelerationInput = _droneInputProvider.AccelerationInput;
            if (_debugTextOn) Debug.LogFormat("Acceleration input received: {0}", accelerationInput);
            AccelerationInputReceived(accelerationInput);

            var rotationInput = _droneInputProvider.RotationInput;
            if (_debugTextOn) Debug.LogFormat("Rotation input received: {0}", rotationInput);
            RotationInputReceived(rotationInput);

            var tiltInputX = _droneInputProvider.TiltInputX;
            var tiltInputY = _droneInputProvider.TiltInputY;
            if (_debugTextOn) Debug.LogFormat("Tilt input received: {0}, {1}", tiltInputX, tiltInputY);
            TiltInputReceived(tiltInputX, tiltInputY);
        }

        private void AccelerationInputReceived(float acceleration)
        {
            OnAccelerationInputReceived?.Invoke(this, acceleration);
        }

        private void RotationInputReceived(float rotation)
        {
            OnRotationInputReceived?.Invoke(this, rotation);
        }

        private void TiltInputReceived(float horizontalTilt, float verticalTilt)
        {
            OnTiltInputReceived?.Invoke(this, new Vector2(horizontalTilt, verticalTilt));
        }
    }

}