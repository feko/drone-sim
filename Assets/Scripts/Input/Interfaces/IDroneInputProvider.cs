﻿namespace droneSim.Input.Interfaces
{
    public interface IDroneInputProvider
    {
        bool ControllerConnected { get; }
        float AccelerationInput { get; }
        float RotationInput { get; }
        float TiltInputX { get; }
        float TiltInputY { get; }
    }
}