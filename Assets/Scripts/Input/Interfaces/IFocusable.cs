﻿namespace droneSim.Input.Interfaces
{
    public interface IFocusable
    {
        void OnFocusGained();
        void OnFocusLost();
    } 
}
