﻿using System;

namespace droneSim.Input.Interfaces
{
    public interface IUIInputEventDispatcher
    {
        event EventHandler OnConfirm;
        event EventHandler OnBack;
        event EventHandler OnLeft;
        event EventHandler OnRight;
        event EventHandler OnPause;
        event EventHandler OnPin;
    }
}
