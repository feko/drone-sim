﻿namespace droneSim.Input.Interfaces
{
    public interface IUIInputProvider
    {
        bool ConfirmActive { get; }
        bool BackActive { get; }
        bool LeftActive { get; }
        bool RightActive { get; }
        bool PauseActive { get; }
        bool PinActive { get; }
    }
}
