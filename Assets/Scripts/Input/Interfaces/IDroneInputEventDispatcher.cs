﻿using System;
using UnityEngine;

namespace droneSim.Input.Interfaces
{
    public interface IDroneInputEventDispatcher
    {
        event EventHandler<float> OnAccelerationInputReceived;
        event EventHandler<Vector2> OnTiltInputReceived;
        event EventHandler<float> OnRotationInputReceived;
    }
}
