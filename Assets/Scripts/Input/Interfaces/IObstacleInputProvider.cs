﻿namespace droneSim.Input.Interfaces
{
    public interface IObstacleInputProvider
    {
        float ForwardBackwardAxis { get; }
        float LeftRightAxis { get; }
        float UpDownAxis { get; }
        float Rotation { get; }
        float ScaleAxis { get; }
    }
}
