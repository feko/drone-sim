﻿using droneSim.Data.Models;
using UnityEngine;

namespace droneSim.Accessors
{
    public class HintAccessor : MonoBehaviour
    {
        [SerializeField]
        private HintType _hintType;

        public HintType HintType { get { return _hintType; } }
    }
}
