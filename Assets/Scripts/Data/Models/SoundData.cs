﻿using UnityEngine;

namespace droneSim.Data.Models
{
    [CreateAssetMenu(fileName = "SoundData", menuName = "Drone Sim/Sound Data")]
    public class SoundData : ScriptableObject
    {
        public Sound[] Sounds;
    } 
}
