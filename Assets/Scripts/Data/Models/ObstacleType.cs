﻿namespace droneSim.Data.Models
{
    public enum ObstacleType
    {
        Loop,
        Wall
    }
}
