﻿using System;
using UnityEngine;

namespace droneSim.Data.Models
{
    [Serializable]
    public class Obstacle
    {
        public ObstacleType ObstacleType;
        public GameObject ObstaclePrefab;
    }
}
