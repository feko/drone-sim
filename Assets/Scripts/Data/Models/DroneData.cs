﻿using UnityEngine;

namespace droneSim.Data.Models
{
    [CreateAssetMenu(fileName = "DroneData", menuName = "Drone Sim/Drone Data")]
    public class DroneData : ScriptableObject
    {
        [Header("Acceleration")]
        public float AccelerationVariation = 5f;
        public float AccelerationModifier = 1f;
        public float AccelerationBase = -Physics.gravity.y;
        public float AccelerationThreshold = 0.1f;

        [Header("Rotation")]
        public float RotationModifier = 1f;
        public float RotationBaseSpeed = 0f;
        public float RotationThreshold = 0.1f;
        public float RotationSpeed = 25f;
        [Range(0f, 1f)]
        public float RotationDrag = 0.1f;
        public float RotationRecoverModifier = 2f;
        public Vector3 RotationAxis = new Vector3(0f, 0f, 1f);

        [Header("Tilt")]
        public Vector2 TiltVariation = Vector2.one;
        public Vector2 TiltModifier = Vector2.one * 0.5f;
        public Vector2 TiltEaseStep = Vector2.one * 0.1f;
        public Vector2 TiltBase = Vector2.zero;
        public Vector2 TiltThreshold = Vector2.one * 0.2f;
        public float MaximumTilt = 25f;
        public Vector3 TiltAxis = new Vector3(1f, 1f, 0f);
        public Vector3 MaximumTiltRotation { get { return MaximumTilt * TiltAxis; } }

    }
}