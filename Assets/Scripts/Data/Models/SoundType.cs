﻿namespace droneSim.Data.Models
{
    public enum SoundType
    {
        Focus,
        Select,
        Error,
        SwingDown,
        SwingUp,
        Delete,
        Place
    } 
}
