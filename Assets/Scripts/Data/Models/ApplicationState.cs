﻿namespace droneSim.Data.Models
{
    public enum ApplicationState
    {
        Idle,
        Menu,
        FlyingDrone,
        PlacingObstacles
    }
}
