﻿using UnityEngine;

namespace droneSim.Data.Models
{
    [CreateAssetMenu(fileName = "ObstacleData", menuName = "Drone Sim/Obstacle Data")]
    public class ObstacleData : ScriptableObject
    {
        public Obstacle[] Obstacles;
    }
}
