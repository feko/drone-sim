﻿namespace droneSim.Data.Models
{
    public enum HintType
    {
        FlyDrone,
        ObstaclePlacing,
        General
    }
}
