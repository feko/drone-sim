﻿using System;
using UnityEngine;

namespace droneSim.Data.Models
{
    [Serializable]
    public class Sound
    {
        public SoundType SoundType;
        public AudioClip SoundClip;
    }
}
